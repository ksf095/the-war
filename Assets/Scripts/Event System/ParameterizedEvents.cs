﻿using Core;
using Datas;
using UnityEngine;
using UnityEngine.Events;

namespace Event_System
{
    [System.Serializable]
    public class FloatEvent : UnityEvent<float>
    {
    }

    [System.Serializable]
    public class StringEvent : UnityEvent<string>
    {
    }

    [System.Serializable]
    public class TeamTypeEvent : UnityEvent<TeamType>
    {
    }

    [System.Serializable]
    public class Vector2Event : UnityEvent<Vector2>
    {
    }

    [System.Serializable]
    public class Vector3Event : UnityEvent<Vector3>
    {
    }


    [System.Serializable]
    public class PairVector3Event : UnityEvent<PairVector3>
    {
    }

    [System.Serializable]
    public class GameObjectEvent : UnityEvent<GameObject>
    {
    }

    [System.Serializable]
    public class PopupEvent : UnityEvent<PopupData>
    {
    }
}