﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;

namespace RoboRyanTron.Unite2017.Variables
{
    /// <summary>
    /// Sets an Image component's fill amount to represent how far Variable is
    /// between Min and Max.
    /// </summary>
    public class TextSetter : MonoBehaviour
    {
        // [Tooltip("Value to use as the current ")]
        // public FloatVariable Variable;

        [Tooltip("Image to set the fill amount on.")]
        public Text text;

        public string stringFormat;

        // private void Update()
        // {
        //     text.text = string.Format(stringFormat, Variable.Value);
        // }

        public void SetValue(float value)
        {
            text.text = string.Format(stringFormat, value);
        }
    }
}