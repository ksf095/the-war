﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using Core;
using Event_System;
using UnityEngine;
using UnityEngine.Events;

namespace RoboRyanTron.Unite2017.Events
{
    public class PairVector3GameEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")] public PairVector3GameEvent Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public PairVector3Event Response;

        private void OnEnable()
        {
            Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            Event.UnregisterListener(this);
        }

        public void OnEventRaised(PairVector3 value)
        {
            Response.Invoke(value);
        }
    }
}