﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using Datas;
using Event_System;
using UnityEngine;
using UnityEngine.Events;

namespace RoboRyanTron.Unite2017.Events
{
    public class PopupGameEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")] public PopupGameEvent Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public PopupEvent Response;

        private void OnEnable()
        {
            Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            Event.UnregisterListener(this);
        }

        public void OnEventRaised(PopupData popupData)
        {
            Response.Invoke(popupData);
        }
    }
}