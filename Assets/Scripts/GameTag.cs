﻿public enum GameTag
{
    Untagged,
    Respawn,
    Finish,
    EditorOnly,
    MainCamera,
    Player,
    GameController,
    Battlefield,
    Warrior,
    WarriorFactory
}