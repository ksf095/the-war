﻿using UnityEngine;

namespace Core
{
    public class InstantiatePrefab : MonoBehaviour
    {
        [SerializeField] private GameObject prefab;

        public void InstantiateLookAt(PairVector3 pairVector3)
        {
            var go = Instantiate(prefab);
            go.transform.position = pairVector3.a;
            go.transform.forward = pairVector3.b;
        }
    }
}