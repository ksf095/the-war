﻿using UnityEngine;

namespace Core
{
    public class MoveToRoot : MonoBehaviour
    {
        [SerializeField] private Transform target;

        public void Move()
        {
            if (!target)
            {
                target = transform;
            }

            target.parent = null;
        }
    }
}