﻿using UnityEngine;

namespace Core
{
    public class Initialization : MonoBehaviour
    {
        private GameConfig _gameConfig;

        private void Awake()
        {
            Config.Initialize();
            GameState.isGameStarted = false;
        }

        private void Start()
        {
            Application.targetFrameRate = 60;
        }
    }
}