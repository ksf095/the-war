﻿using UnityEngine;

namespace Core
{
    public static class Stopwatch
    {
        private static float _startTime;

        public static void StartStopwatch()
        {
            _startTime = Time.timeSinceLevelLoad;
        }

        public static float GetCurrentStopwatchTime()
        {
            return Time.timeSinceLevelLoad - _startTime;
        }
    }
}