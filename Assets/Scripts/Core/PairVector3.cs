﻿using UnityEngine;

namespace Core
{
    public class PairVector3
    {
        public Vector3 a;
        public Vector3 b;

        public PairVector3(Vector3 a, Vector3 b)
        {
            this.a = a;
            this.b = b;
        }
    }
}
