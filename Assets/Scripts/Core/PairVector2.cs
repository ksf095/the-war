﻿using UnityEngine;

namespace Core
{
    public class PairVector2
    {
        public Vector2 a;
        public Vector2 b;

        public PairVector2(Vector2 a, Vector2 b)
        {
            this.a = a;
            this.b = b;
        }
    }
}