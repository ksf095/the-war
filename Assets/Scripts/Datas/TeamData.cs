﻿using System;
using UnityEngine;

namespace Datas
{
    [Serializable]
    public class TeamData
    {
        public TeamType teamType;
        public Material material;
        public LayerMask layer;
    }
}