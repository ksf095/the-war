﻿namespace Datas
{
    public abstract class PopupData
    {
        public virtual string PrefabName { get; }
    }
}