﻿namespace Datas
{
    public class GameOverPopupData : PopupData
    {
        public readonly float time;
        public readonly TeamType winner;

        public override string PrefabName { get; } = "Game Over Popup";

        public GameOverPopupData(float time, TeamType winner)
        {
            this.time = time;
            this.winner = winner;
        }
    }
}