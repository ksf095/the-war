﻿using System;
using UnityEngine;

namespace Datas
{
    [Serializable]
    public class WarriorData
    {
        public Vector3 position;
        public Vector3 rotation;
        public Vector3 scale;

        public float mass;
        public bool isKinematic;
        public Vector3 velocity;

        public float radius;
        public TeamType team;

        public WarriorData(Vector3 position, Vector3 rotation, Vector3 scale, float mass, bool isKinematic,
            Vector3 velocity, float radius, TeamType team)
        {
            this.position = position;
            this.rotation = rotation;
            this.scale = scale;
            this.mass = mass;
            this.isKinematic = isKinematic;
            this.velocity = velocity;
            this.radius = radius;
            this.team = team;
        }
    }
}