﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Datas
{
    [Serializable]
    public class SaveData
    {
        [SerializeField] public Vector2 fieldSize;
        [SerializeField] public List<WarriorData> list;

        public SaveData(Vector2 fieldSize, List<WarriorData> list)
        {
            this.fieldSize = fieldSize;
            this.list = list;
        }
    }
}