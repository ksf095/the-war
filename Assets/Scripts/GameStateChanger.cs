﻿using Core;
using Datas;
using Event_System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStateChanger : MonoBehaviour
{
    [SerializeField] private PopupEvent onGameOverPopup;

    public void OnStartGame()
    {
        GameState.isGameStarted = true;
        Stopwatch.StartStopwatch();
    }

    public void OnWinnerCause(TeamType team)
    {
        var roundTime = Stopwatch.GetCurrentStopwatchTime();
        var overPopupData = new GameOverPopupData(roundTime, team);

        onGameOverPopup.Invoke(overPopupData);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}