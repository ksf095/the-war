﻿using System;
using UnityEngine;

[Serializable]
public class GameConfig
{
    [SerializeField] private float gameAreaWidth;
    [SerializeField] private float gameAreaHeight;
    [SerializeField] private int numUnitsToSpawn;
    [SerializeField] private float unitSpawnDelay;
    [SerializeField] private float unitSpawnMinRadius;
    [SerializeField] private float unitSpawnMaxRadius;
    [SerializeField] private float unitSpawnMinSpeed;
    [SerializeField] private float unitSpawnMaxSpeed;
    [SerializeField] private float unitDestroyRadius;

    public float GameAreaWidth
    {
        get => gameAreaWidth;
        private set => gameAreaWidth = value;
    }

    public float GameAreaHeight
    {
        get => gameAreaHeight;
        private set => gameAreaHeight = value;
    }

    public int NumUnitsToSpawn
    {
        get => numUnitsToSpawn;
        private set => numUnitsToSpawn = value;
    }

    public float UnitSpawnDelay
    {
        get => unitSpawnDelay;
        private set => unitSpawnDelay = value;
    }

    public float UnitSpawnMinRadius
    {
        get => unitSpawnMinRadius;
        private set => unitSpawnMinRadius = value;
    }

    public float UnitSpawnMaxRadius
    {
        get => unitSpawnMaxRadius;
        private set => unitSpawnMaxRadius = value;
    }

    public float UnitSpawnMinSpeed
    {
        get => unitSpawnMinSpeed;
        private set => unitSpawnMinSpeed = value;
    }

    public float UnitSpawnMaxSpeed
    {
        get => unitSpawnMaxSpeed;
        private set => unitSpawnMaxSpeed = value;
    }

    public float UnitDestroyRadius
    {
        get => unitDestroyRadius;
        private set => unitDestroyRadius = value;
    }
}