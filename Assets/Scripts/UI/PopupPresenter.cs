﻿using Datas;
using UnityEngine;

namespace UI
{
    public abstract class PopupPresenter : MonoBehaviour
    {
        public virtual void SetInfo(PopupData popupData) { }
    }
}