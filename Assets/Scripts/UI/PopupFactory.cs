﻿using Datas;
using UnityEngine;

namespace UI
{
    public class PopupFactory : MonoBehaviour
    {
        public void CreatePopup(PopupData popupData)
        {
            var popupGameObject = Instantiate(Resources.Load<GameObject>(popupData.PrefabName), transform);
       
            if (popupGameObject == null)
                return;

            var popup = popupGameObject.GetComponent<PopupPresenter>();
            if (popup != null)
            {
                popup.SetInfo(popupData);
            }
        }
    }
}