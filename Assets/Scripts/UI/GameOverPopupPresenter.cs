﻿using Datas;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class GameOverPopupPresenter : PopupPresenter
    {
        [SerializeField] private string timeTemplate = "The game lasted {0}s";
        [SerializeField] private string winnerTemplate = "{0} team won!";

        [SerializeField] private Text timeText;
        [SerializeField] private Text teamText;

        public override void SetInfo(PopupData popupData)
        {
            var data = (GameOverPopupData) popupData;
            if (data == null)
            {
                return;
            }

            timeText.text = string.Format(timeTemplate, Mathf.Floor(data.time));
            teamText.text = string.Format(winnerTemplate, data.winner);
        }
    }
}