﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    public class UiHelper : MonoBehaviour
    {
        public static bool IsInteractingWithUi()
        {
            Vector2 position = Input.mousePosition;
            var pointerData = new PointerEventData(EventSystem.current);
            var resultsData = new List<RaycastResult>();
            pointerData.position = position;
            EventSystem.current.RaycastAll(pointerData, resultsData);

            return resultsData.Count > 0;
        }
    }
}