﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Datas;
using UnityEngine;

public static class TeamManager
{
    public static Teams TeamDatas { get; private set; }

    [RuntimeInitializeOnLoadMethod]
    private static void ObtainTeams()
    {
        TeamDatas = Resources.Load<Teams>("Teams");
    }
    
    public static TeamData GetTeamData(TeamType teamType)
    {
        return TeamDatas.teamDatas.FirstOrDefault(x => x.teamType == teamType);
    }
  
}