﻿using UnityEngine;

public static class Config
{
    public static GameConfig gameConfig;


    public static void Initialize()
    {
        gameConfig =
            JsonUtility.FromJson<GameConfig>(Resources.Load<TextAsset>(path: Constants.GameConfigPath).ToString());
    }
}