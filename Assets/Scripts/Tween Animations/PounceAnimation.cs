﻿using DG.Tweening;
using UnityEngine;

namespace Tween_Animations
{
    public class PounceAnimation : MonoBehaviour
    {
        [SerializeField] private float duration = 0.5f;
        [SerializeField] private float fromValue = 0.9f;
        [SerializeField] private float endValue = 1;

        private void Start()
        {
            var mySequence = DOTween.Sequence();

            mySequence.Append(transform.DOScale(fromValue, duration).From())
                .Append(transform.DOScale(endValue, duration).From())
                .SetLoops(-1);
        }
    }
}