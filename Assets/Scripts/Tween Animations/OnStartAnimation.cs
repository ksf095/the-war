﻿using DG.Tweening;
using UnityEngine;

namespace Tween_Animations
{
    public class OnStartAnimation : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private float time = 1f;
        [SerializeField] private Ease ease = Ease.InCirc;

        private void Start()
        {
            if (target == null)
            {
                target = transform;
            }

            transform.DOScale(Vector3.zero, time).From().SetEase(ease);
        }
    }
}