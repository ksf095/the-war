﻿using Cinemachine;
using UnityEngine;

namespace Camera
{
    public class CineMachineSetRigs : MonoBehaviour
    {
        private CinemachineFreeLook _cinemachineFreeLook;

        [SerializeField] private float _lowOrbitRigHeight = 7.4f;
        [SerializeField] private float _lowOrbitRadiusMultiplier = 1.175f;
        [SerializeField] private float _topRigRadius = 1.2f;
        [SerializeField] private float _mediumOrbitRadiusMultiplier = 2.085f;

        private void Start()
        {
            _cinemachineFreeLook = GetComponent<CinemachineFreeLook>();
        }

        public void SetOrbits(Vector2 fieldSize)
        {
            var fieldSide = Mathf.Max(fieldSize.x, fieldSize.y);

            _cinemachineFreeLook.m_Orbits = new CinemachineFreeLook.Orbit[3]
            {
                new CinemachineFreeLook.Orbit(GetTopRigHeight(fieldSide), _topRigRadius),
                new CinemachineFreeLook.Orbit(GetMediumRigHeight(fieldSide), fieldSide * _mediumOrbitRadiusMultiplier),
                new CinemachineFreeLook.Orbit(_lowOrbitRigHeight, fieldSide * _lowOrbitRadiusMultiplier)
            };
        }

        private float GetTopRigHeight(float value)
        {
            return 1.73f * value + 4.7f;
        }

        private float GetMediumRigHeight(float value)
        {
            return _lowOrbitRigHeight + (GetTopRigHeight(value) - _lowOrbitRigHeight) / 2;
        }
    }
}