﻿using Cinemachine;
using UI;
using UnityEngine;

namespace Camera
{
    public class OverrideCineMachineControl : MonoBehaviour
    {
        private void Start()
        {
            CinemachineCore.GetInputAxis = GetAxisCustom;
        }

        public float GetAxisCustom(string axisName)
        {
            if (UiHelper.IsInteractingWithUi())
            {
                return 0;
            }

            if (axisName == "Mouse X")
            {
                if (Input.GetMouseButton(0))
                {
                    return Input.GetAxis("Mouse X");
                }

                return 0;
            }

            if (axisName == "Mouse Y")
            {
                if (Input.GetMouseButton(0))
                {
                    return Input.GetAxis("Mouse Y");
                }

                return 0;
            }

            return Input.GetAxis(axisName);
        }
    }
}