﻿using Cinemachine;
using UnityEngine;

namespace Camera
{
    public class ApplyToTargetGroup : MonoBehaviour
    {
        private CinemachineTargetGroup _cinemachineTargetGroup;

        private void Awake()
        {
            _cinemachineTargetGroup = GetComponent<CinemachineTargetGroup>();
        }

        public void AddCameraTarget(GameObject target)
        {
            _cinemachineTargetGroup.AddMember(target.transform, 1, 1);
        }
    }
}