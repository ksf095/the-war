﻿using Datas;

namespace Interfaces
{
    public interface ISerializable
    {
        WarriorData Save();
        void Load(WarriorData warriorData);
        void RegisterInSaveSystem();
        void UnRegisterInSaveSystem();
        void Destroy();
    
    }
}