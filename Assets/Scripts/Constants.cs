﻿public class Constants
{
    public static readonly string PlayerPrefsSavedSimulation = "Saved Simulation";
    public static readonly string PlayerPrefsDefaultData = "{\"fieldSize\":{\"x\":0.0,\"y\":0.0},\"list\":[]}";
    
    public static readonly string GameConfigPath = "GameConfig";
}