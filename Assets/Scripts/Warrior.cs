﻿using Core;
using Datas;
using Event_System;
using Interfaces;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;


public class Warrior : MonoBehaviour, ISerializable
{
    [SerializeField] private UnityEvent onKill;
    [SerializeField] private PairVector3Event onCollisionEnter;
    [SerializeField] private FloatEvent onRadiusChanged;
    [SerializeField] private UnityEvent onTeamChanged;

    [SerializeField] private float damage;

    private Rigidbody _rigidbody;
    private MeshRenderer _meshRenderer;
    private Material _material;

    private TeamType _team;
    private TeamData _teamData;

    private float _radius;

    public TeamType Team
    {
        get => _team;
        set
        {
            _team = value;
            onTeamChanged.Invoke();
        }
    }

    public float Radius
    {
        get => _radius;
        set
        {
            _radius = value;
            onRadiusChanged.Invoke(Radius);
        }
    }

    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision other)
    {
        for (var index = 0; index < other.contacts.Length; index++)
        {
            var contact = other.contacts[index];
            if (contact.otherCollider.CompareTag(GameTag.Warrior.ToString()))
            {
                onCollisionEnter.Invoke(new PairVector3(contact.point, contact.normal));
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        var warrior = other.GetComponent<Warrior>();

        if (!warrior)
            return;

        if (!Equals(Team, warrior.Team))
        {
            GetDamage(damage);
        }
    }

    public void ObtainTeamData()
    {
        _teamData = TeamManager.GetTeamData(_team);
    }

    public void UpdateMaterial()
    {
        _meshRenderer.material = _teamData.material;
    }

    public void UpdateLayer()
    {
        gameObject.layer = Mathf.RoundToInt(Mathf.Log(_teamData.layer.value, 2));
    }

    private void GetDamage(float damage)
    {
        Radius -= damage;
    }

    public void SetSize(float radius)
    {
        transform.localScale = Vector3.one * radius * 2;

        var position = transform.position;
        position.y = radius;
        transform.position = position;

        _rigidbody.mass = 4f / 3f * Mathf.PI * radius * radius * radius;
    }

    public void CheckDeath()
    {
        if (_radius < Config.gameConfig.UnitDestroyRadius)
        {
            Kill();
        }
    }

    private void Kill()
    {
        onKill.Invoke();
    }

    private void SetMoveVector(Vector3 vector)
    {
        _rigidbody.velocity = vector;
    }

    public void MoveRandomDirection()
    {
        var moveVector = Random.insideUnitSphere.normalized;
        moveVector.y = 0;

        SetMoveVector(moveVector *
                      Random.Range(Config.gameConfig.UnitSpawnMinSpeed, Config.gameConfig.UnitSpawnMaxSpeed));
    }

    public void RegisterInSaveSystem()
    {
        SaveSystem.AddSaveable(this);
    }

    public void UnRegisterInSaveSystem()
    {
        SaveSystem.RemoveSaveable(this);
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }

    public WarriorData Save()
    {
        var warriorData = new WarriorData(transform.position, transform.rotation.eulerAngles, transform.localScale,
            _rigidbody.mass, _rigidbody.isKinematic, _rigidbody.velocity, Radius, Team);

        return warriorData;
    }

    public void Load(WarriorData warriorData)
    {
        transform.position = warriorData.position;
        transform.rotation = Quaternion.Euler(warriorData.rotation);
        transform.localScale = warriorData.scale;
        _rigidbody.mass = warriorData.mass;
        _rigidbody.isKinematic = warriorData.isKinematic;
        _rigidbody.velocity = warriorData.velocity;
        Radius = warriorData.radius;
        Team = warriorData.team;
    }
}