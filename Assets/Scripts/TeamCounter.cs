﻿using System.Collections.Generic;
using Event_System;
using RoboRyanTron.Unite2017.Variables;
using UnityEngine;

public class TeamCounter : MonoBehaviour
{
    [SerializeField] private TeamTypeEvent onWinnerCause;

    [SerializeField] private FloatVariable teamBalance;

    private readonly List<Warrior> _blueTeam = new List<Warrior>();
    private readonly List<Warrior> _redTeam = new List<Warrior>();

    public void UpdateTeamBalance()
    {
        float blueTeamSize = 0;
        for (var i = 0; i < _blueTeam.Count; i++)
        {
            blueTeamSize += _blueTeam[i].Radius;
        }

        float redTeamSize = 0;
        for (var i = 0; i < _redTeam.Count; i++)
        {
            redTeamSize += _redTeam[i].Radius;
        }

        var value = blueTeamSize / (blueTeamSize + redTeamSize);

        if (!double.IsNaN(value))
        {
            teamBalance.Value = value;
        }
    }

    public void OnAddedParticipant(GameObject go)
    {
        var warrior = go.GetComponent<Warrior>();

        if (!warrior)
        {
            return;
        }

        switch (warrior.Team)
        {
            case TeamType.Blue:
                if (!_blueTeam.Contains(warrior))
                {
                    _blueTeam.Add(warrior);
                }

                break;

            case TeamType.Red:
                if (!_redTeam.Contains(warrior))
                {
                    _redTeam.Add(warrior);
                }

                break;
        }

        UpdateTeamBalance();
    }

    private void Start()
    {
        teamBalance.Value = 0.5f;
    }

    private void CheckIsGameOver()
    {
        if (!GameState.isGameStarted)
            return;

        if (_blueTeam.Count == 0)
        {
            onWinnerCause.Invoke(TeamType.Red);
        }
        else if (_redTeam.Count == 0)
        {
            onWinnerCause.Invoke(TeamType.Blue);
        }
    }

    public void OnDestroyedParticipant(GameObject go)
    {
        var warrior = go.GetComponent<Warrior>();

        if (!warrior)
        {
            return;
        }

        switch (warrior.Team)
        {
            case TeamType.Blue:
                _blueTeam.Remove(warrior);
                break;

            case TeamType.Red:
                _redTeam.Remove(warrior);
                break;
        }

        UpdateTeamBalance();
        CheckIsGameOver();
    }
}