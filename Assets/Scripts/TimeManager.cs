﻿using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public void SetTimeScale(float value)
    {
        Time.timeScale = value;
    }

    private void Start()
    {
        Time.timeScale = 1;
    }
}