﻿using Core;
using Datas;
using Event_System;
using UnityEngine;

public class Battlefield : MonoBehaviour
{
    [SerializeField] private Vector2Event onCreateField;

    [SerializeField] private GameObject wallPrefab;

    [SerializeField] private float wallWidth = 3;

    [Header("Wall Rotations")] [SerializeField]
    private Vector3 leftWallRotation = new Vector3(0, 0, 0);

    [SerializeField] private Vector3 topWallRotation = new Vector3(0, 90, 0);
    [SerializeField] private Vector3 rightWallRotation = new Vector3(0, 180, 0);
    [SerializeField] private Vector3 bottomWallRotation = new Vector3(0, 270, 0);

    private readonly GameObject[] _walls = new GameObject[4];

    private Vector2 _battlefieldSize;
    private Vector3 _verticalWallSize;
    private Vector3 _horizontalWallSize;

    private void Start()
    {
        CreateField(ObtainFieldParameters());
    }

    private Vector2 ObtainFieldParameters()
    {
        return new Vector2(Mathf.Max(Config.gameConfig.GameAreaWidth, 3),
            Mathf.Max(Config.gameConfig.GameAreaHeight, 3));
    }

    private void CreateField(Vector2 battlefieldSize)
    {
        ObtainFieldParameters();

        _verticalWallSize = new Vector3(wallWidth, wallWidth, battlefieldSize.y + 2 * wallWidth);
        _horizontalWallSize = new Vector3(wallWidth, wallWidth, battlefieldSize.x + 2 * wallWidth);

        _walls[0] = CreateWall(new Vector3(transform.position.x - battlefieldSize.x / 2 - wallWidth / 2,
            transform.localScale.y, 0), _verticalWallSize, Quaternion.Euler(leftWallRotation));

        _walls[1] = CreateWall(
            new Vector3(0, transform.localScale.y, transform.position.z + battlefieldSize.y / 2 + wallWidth / 2),
            _horizontalWallSize, Quaternion.Euler(topWallRotation));

        _walls[2] = CreateWall(
            new Vector3(transform.position.x + battlefieldSize.x / 2 + wallWidth / 2, transform.localScale.y, 0),
            _verticalWallSize, Quaternion.Euler(rightWallRotation));

        _walls[3] = CreateWall(
            new Vector3(0, transform.localScale.y, transform.position.z - battlefieldSize.y / 2 - wallWidth / 2),
            _horizontalWallSize, Quaternion.Euler(bottomWallRotation));

        _battlefieldSize = battlefieldSize;
        onCreateField.Invoke(new Vector2(battlefieldSize.x, battlefieldSize.y));
    }

    public PairVector2 GetFieldBounds()
    {
        return new PairVector2(
            new Vector2(transform.position.x - _battlefieldSize.x / 2, transform.position.y + _battlefieldSize.y / 2),
            new Vector2(transform.position.x + _battlefieldSize.x / 2, transform.position.y - _battlefieldSize.y / 2));
    }

    private GameObject CreateWall(Vector3 position, Vector3 size, Quaternion rotation)
    {
        var wall = Instantiate(wallPrefab);
        wall.transform.position = position;
        wall.transform.localScale = size;
        wall.transform.rotation = rotation;

        return wall;
    }

    public void OnLoadGame()
    {
        SaveData data =
            JsonUtility.FromJson<SaveData>(PlayerPrefs.GetString(Constants.PlayerPrefsSavedSimulation,
                Constants.PlayerPrefsDefaultData));


        for (var i = 0; i < _walls.Length; i++)
        {
            Destroy(_walls[i]);
        }

        CreateField(data.fieldSize);
    }
}