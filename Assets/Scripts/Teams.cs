﻿using Datas;
using UnityEngine;

[CreateAssetMenu]
public class Teams : ScriptableObject
{
    public TeamData[] teamDatas;
}