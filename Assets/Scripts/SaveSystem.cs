﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Core;
using Datas;
using Interfaces;
using UnityEngine;

public class SaveSystem : MonoBehaviour
{
    static List<ISerializable> saveAblesWarriors = new List<ISerializable>();

    static readonly GameTag WarriorFactoryTag = GameTag.WarriorFactory;

    private static Vector2 _fieldSize;

    public static void AddSaveable(ISerializable saveAble)
    {
        if (!saveAblesWarriors.Contains(saveAble))
        {
            saveAblesWarriors.Add(saveAble);
        }
    }

    public void SetFieldSize(Vector2 value)
    {
        _fieldSize = value;
    }

    public static void RemoveSaveable(ISerializable saveAble)
    {
        saveAblesWarriors.Remove(saveAble);
    }

    public void SaveAllWrapper()
    {
        SaveAll();
    }

    public void LoadAllWrapper()
    {
        LoadAll();
    }

    private static void SaveAll()
    {
        var list = new List<WarriorData>();
        for (var i = 0; i < saveAblesWarriors.Count; i++)
        {
            list.Add(saveAblesWarriors[i].Save());
        }

        var data = new SaveData(_fieldSize, list);

        PlayerPrefs.SetString(Constants.PlayerPrefsSavedSimulation, JsonUtility.ToJson(data));
    }

    private static void LoadAll()
    {
        GameState.isGameStarted = false;

        var tempArray = saveAblesWarriors.ToArray();
        for (var i = 0; i < tempArray.Length; i++)
        {
            tempArray[i].Destroy();
        }

        var warriorFactoryGameObject = GameObject.FindWithTag(WarriorFactoryTag.ToString());
        if (!warriorFactoryGameObject)
        {
            return;
        }

        var warriorFactory = warriorFactoryGameObject.GetComponent<WarriorFactory>();
        if (!warriorFactory)
            return;

        SaveData data =
            JsonUtility.FromJson<SaveData>(PlayerPrefs.GetString(Constants.PlayerPrefsSavedSimulation,
                Constants.PlayerPrefsDefaultData));

        warriorFactory.SpawnSavedWarriors(data);

        GameState.isGameStarted = true;
    }
}