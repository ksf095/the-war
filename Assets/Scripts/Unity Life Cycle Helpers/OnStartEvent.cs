﻿using UnityEngine;
using UnityEngine.Events;

namespace Unity_Life_Cycle_Helpers
{
    public class OnStartEvent : MonoBehaviour
    {
        [SerializeField] private UnityEvent unityEvent;

        private void Start()
        {
            unityEvent.Invoke();
        }
    }
}