﻿using UnityEngine;
using UnityEngine.Events;

namespace Unity_Life_Cycle_Helpers
{
    public class OnDisableEvent : MonoBehaviour
    {
        [SerializeField] private UnityEvent unityEvent;

        private void OnDisable()
        {
            unityEvent.Invoke();
        }
    }
}