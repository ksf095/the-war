﻿using UnityEngine;
using UnityEngine.Events;

namespace Unity_Life_Cycle_Helpers
{
    public class OnDestroyEvent : MonoBehaviour
    {
        [SerializeField] private UnityEvent unityEvent;

        private void OnDestroy()
        {
            unityEvent.Invoke();
        }
    }
}