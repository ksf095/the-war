﻿using UnityEngine;
using UnityEngine.Events;

namespace Unity_Life_Cycle_Helpers
{
    public class OnEnableEvent : MonoBehaviour
    {
        [SerializeField] private UnityEvent unityEvent;

        private void OnEnable()
        {
            unityEvent.Invoke();
        }
    }
}