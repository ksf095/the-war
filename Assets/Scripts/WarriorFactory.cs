﻿using System;
using System.Collections;
using System.Linq;
using Datas;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class WarriorFactory : MonoBehaviour
{
    [SerializeField] private UnityEvent onEndSpawnWarriors;
    [SerializeField] private GameObject warriorPrefab;
    [SerializeField] private LayerMask warriorLayerMask;

    private Battlefield _battlefield;

    private Coroutine _spawningWarriors;

    private Battlefield GetBattleField()
    {
        return GameObject.FindWithTag(GameTag.Battlefield.ToString()).GetComponent<Battlefield>();
    }

    public void SpawnWarriors()
    {
        _spawningWarriors = StartCoroutine(SpawnWarriorsContinious());
    }

    private IEnumerator SpawnWarriorsContinious()
    {
        _battlefield = GetBattleField();

        if (_battlefield == null)
        {
            yield break;
        }

        var fieldBounds = _battlefield.GetFieldBounds();

        var amountWarriorsInTeam = Config.gameConfig.NumUnitsToSpawn;
        var minRadius = Config.gameConfig.UnitSpawnMinRadius;
        var maxRadius = Config.gameConfig.UnitSpawnMaxRadius;

        //get all teams
        var allTeams = Enum.GetValues(typeof(TeamType)).Cast<TeamType>().ToList();

        for (int i = 0; i < amountWarriorsInTeam; i++)
        {
            for (int team = 0; team < allTeams.Count; team++)
            {
                var forceExit = 0;
                while (true)
                {
                    var radius = Random.Range(minRadius, maxRadius);

                    //Generate random point inside of the battlefield
                    var point = new Vector3(Random.Range(fieldBounds.a.x + radius, fieldBounds.b.x - radius), radius,
                        Random.Range(fieldBounds.b.y + radius, fieldBounds.a.y - radius));

                    if (!Physics.CheckSphere(point, radius, warriorLayerMask))
                    {
                        InstantiateWarrior(point, radius, allTeams[team]);
                        break;
                    }

                    if (forceExit++ == 100)
                    {
                        break;
                    }
                }

                yield return new WaitForSecondsRealtime(Config.gameConfig.UnitSpawnDelay);
            }
        }

        onEndSpawnWarriors.Invoke();
    }

    private Warrior InstantiateWarrior(Vector3 position = new Vector3(), float radius = 1,
        TeamType team = TeamType.Blue)
    {
        var warriorGameObject = Instantiate(warriorPrefab, position, transform.rotation);

        var warrior = warriorGameObject.GetComponent<Warrior>();
        warrior.Radius = radius;
        warrior.Team = team;
        return warrior;
    }

    public void SpawnSavedWarriors(SaveData saveData)
    {
        for (var i = 0; i < saveData.list.Count; i++)
        {
            var warrior = InstantiateWarrior();
            warrior.Load(saveData.list[i]);
        }
    }

    public void StopSpawningWarriors()
    {
        if (_spawningWarriors != null)
        {
            StopCoroutine(_spawningWarriors);
        }
    }
}